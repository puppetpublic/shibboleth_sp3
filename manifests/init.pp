# This module gives you a Shibboleth 3.x service provider.  Additional
# configuration is required for this to actually work. In particular, this
# Puppet module makes no attempt to configure shibboleth2.xml.

# [WHERE IS THIS USED?!?]
# $include_apache_ssl: set to true to have this module include the Apache
#   module ssl. If the calling class already does this, set this parameter
#   to false to avoid duplicate resource errors.
#   Default: true

# In buster the Apache shibboleth module is called "shib" but in stretch
# (non-backports) the module is called "shib2". Confusing. To get around
# this, allow the module name to be overridden by the parameter
# $apache_shib_mod_name.

class shibboleth_sp3 (
  Boolean $include_apache_ssl     = true,
  Boolean $enable_newsyslog_shibd = true,
  String  $apache_shib_mod_name   = 'shib',
) {

  include shibboleth_sp3::params

  if ($shibboleth_sp3::params::use_su_apache) {
    include su_apache
  } else {
    include apache
  }

  # Create the shibd user
  include user::shibd

  # The shibd service communicates via a Unix socket. Be default, on Debian
  # it will try to put the socket in /run/shibboleth. Since /run is only
  # writable by root and shibd runs as _shibd, the shibd service will not
  # be able to create /run/shibboleth on its own. So, we make it
  # ourselves. The directory must be owned by _shibd.
  file { '/run/shibboleth':
    ensure  => directory,
    owner   => '_shibd',
    group   => '_shibd',
    require => Class['user::shibd'],
  }

  # Install the Shibboleth packages.
  $os_distro_id = $facts['os']['distro']['id']

  case $os_distro_id {
    'Debian', 'Ubuntu': {
      package { 'libapache2-mod-shib':
        ensure  => present,
      }

      # Enable the required Apache modules.
      if ($shibboleth_sp3::params::use_su_apache) {
        su_apache::module {$apache_shib_mod_name: ensure => present }
      } else {
        apache::module {$apache_shib_mod_name: ensure => present }
      }

      # Manage shibd logs file ownership
      $shibd_logs = [ '/var/log/shibboleth/shibd.log',
                      '/var/log/shibboleth/shibd_warn.log',
                      '/var/log/shibboleth/signature.log',
                      '/var/log/shibboleth/transaction.log' ]

      file { $shibd_logs:
        ensure  => present,
        owner   => '_shibd',
        group   => '_shibd',
        require => Class['user::shibd'],
      }

      service { 'shibd':
        ensure  => running,
        require => Package['libapache2-mod-shib'],
      }

    } # end of case 'debian'
    'redhat': {
      package { 'shibboleth': ensure => present }
      base::rpm::import { 'shib-rpmkey':
        url       => 'http://yum.stanford.edu/RPM-GPG-KEY-SHIB',
        signature => 'gpg-pubkey-7d0a1b3d-48689588';
      }
      case $::lsbdistcodename {
        'tikanga': {
          file { '/etc/yum.repos.d/shib.repo':
            source => 'puppet:///modules/shibboleth_sp3/etc/yum/shib.repo'
          }
          # Make sure the shibd daemon is running.
          service { 'shibd':
            ensure    => running,
            hasstatus => false,
            status    => 'pidof shibd',
            require   => Package['shibboleth'],
          }
        }
      }
    } # end of case 'redhat'
    default: { crit("cannot handle operating system ${::operatingsystem} yet") }
  }

  # We want to manage the /etc/apache2/conf-available/shib.conf file. The
  # main reason for wanting to manage it is that the default installation
  # from the Debian package sets the ShibCompatValidUser directive to
  # "Off" whereas we want it set to "On" so that other authentication
  # packages work properly, e.g., basic auth (see also
  # https://wiki.shibboleth.net/confluence/display/SP3/htaccess).
  file { '/etc/apache2/conf-available/shib.conf':
    ensure => present,
    source => 'puppet:///modules/shibboleth_sp3/etc/apache2/conf-available/shib.conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  # Change the native.logger configuration, used by the Shibboleth libraries
  # embedded in Apache, to log via syslog instead of logging to files.
  file { '/etc/shibboleth/native.logger':
    source => 'puppet:///modules/shibboleth_sp3/etc/shibboleth/native.logger',
  }

  # Rotate the Shibboleth logs.
  if ($enable_newsyslog_shibd) {
    file {
      '/etc/newsyslog.daily/shibd':
        source => 'puppet:///modules/shibboleth_sp3/etc/newsyslog.daily/shibd';
      '/var/log/shibboleth/OLD':
        ensure => directory,
        mode   => '0750';
    }
  }

  # Install log filtering rules.
  file { '/etc/filter-syslog/shibboleth-sp':
    source => 'puppet:///modules/shibboleth_sp3/etc/filter-syslog/shibboleth-sp',
  }
}
