# Manage a simple shibboleth2.xml file
#
# $cert_name
# $key_wallet_name

class shibboleth_sp3::shibboleth2_xml (
  String $sp_entityid,
  String $support_contact,
  #
  String $idp_entityid                   = 'https://idp.stanford.edu/',
  String $idp_metadata_url               = 'https://idp.stanford.edu/metadata.xml',
  String $metadata_backingfile_directory = '/var/tmp/',
  #
  String $attribute_map_path  = 'attribute-map.xml',
  #
  String $shib_name,
  String $shib_cert         = '/etc/shibboleth/saml-crt.pem',
  String $shib_private_key  = '/etc/shibboleth/saml-key.pem',
  #
  String $cert_name        = "${shib_name}-shib",
  String $key_wallet_name  = "ssl-key/${shib_name}/shib",
  #
  String $authentication_context = 'https://refeds.org/profile/mfa',
  #
  Boolean $versioned_data_sealer = false,
  #
  Boolean $support_saml1         = false,
  #
  # Eventually this should default to true:
  Boolean $manage_attribute_map  = false,
  #
  # Eventually this should default to '3.0':
  Enum['2.0', '3.0'] $shib_namespace = '2.0',
) {

  file { '/etc/shibboleth/shibboleth2.xml':
    content => template('shibboleth_sp3/etc/shibboleth/shibboleth2.xml.erb'),
    notify  => Service['shibd'],
  }

  su_certs::saml { $cert_name:
    ensure    => present,
    identity  => $cert_name,
    keyname   => $key_wallet_name,
    cert_path => $shib_cert,
    key_path  => $shib_private_key,
    notify    => Service['shibd'],
  }

  if ($manage_attribute_map) {
    file { '/etc/shibboleth/attribute-map.xml':
      content => template('shibboleth_sp3/etc/shibboleth/attribute-map.xml.erb'),
      notify => Service['shibd'],
    }
  }
}
