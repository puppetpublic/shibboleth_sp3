[[_TOC_]]

# The shibboleth_sp3 Puppet module

## Overview

This module gives you a Shibboleth 3.x service provider.  Additional
configuration is required for this to actually work. In particular, the
base `shibboleth_sp3` class makes no attempt to configure
`shibboleth2.xml` (but see the ["shibboleth2.xml configuration"
section](#shibboleth2xml-configuration-optional) on one way to manage the
`shibboleth2.xml` file.

## Configuration

### Prerequisites

This module depends upon the su_certs Puppet module found [here](https://code.stanford.edu/suitpuppet/su_certs)

### Basic configuration

* DEPRECATED to use su_certs::saml instead `params::use_su_apache`: normally this module will use
the (legacy) `apache` Puppet module. Set this value to `true` to use the
newer (and better-supported) [su_apache Puppet
module](https://code.stanford.edu/suitpuppet/su_apache). Default: `false`.

* `enable_newsyslog_shibd`: set this `true` to install a newsyslog file rotation
file for the shibd service; note that the shibboleth_sp3 Puppet module will not
attempt to install or manage any other part of newsyslog. Set to `false` to
_not_ install the shibd newsyslog configuration. Default: `true`.


### `shibboleth2.xml` configuration (optional)

Use the `shibboleth_sp3::shibboleth2_xml` Puppet class to manage the file
`/etc/shibboleth/shibboleth2.xml`. Note that using
`shibboleth_sp3::shibboleth2_xml` is entirely optional; if you choose
_not_ to use it you will have to manage `/etc/shibboleth/shibboleth2.xml`
yourself.

In order to invoke the shibboleth2_xml file management add:
```
include shibboleth_sp3::shibboleth2_xml
```
to your puppet code in addition to the hiera settings below:


Example of use:
```
shibboleth_sp3::params::use_su_apache:             true
shibboleth_sp3::shibboleth2_xml::sp_entityid:      https://example.stanford.edu
shibboleth_sp3::shibboleth2_xml::support_contact:   adamhl@stanford.edu
shibboleth_sp3::shibboleth2_xml::shib_name:        example.stanford.edu
shibboleth_sp3::shibboleth2_xml::shib_private_key: /etc/ssl/private/example.stanford.edu-saml.key
shibboleth_sp3::shibboleth2_xml::shib_cert:        /etc/ssl/certs/example.stanford.edu-saml.pem
shibboleth_sp3::shibboleth2_xml::authentication_context: https://saml.stanford.edu/profile/mfa/forced
shibboleth_sp3::shibboleth2_xml::cert_name:        example.stanford.edu-saml
shibboleth_sp3::shibboleth2_xml::key_wallet_name:  ssl-key/example.stanford.edu-saml
shibboleth_sp3::shibboleth2_xml::manage_attribute_map: true
```

You need only supply the parameters marked "[**required**]". For the others, the default
values will probably be fine.


#### General settings

* `sp_entityid`: [**required**] the SP entityID.

* `support_contact`: [**required**] the e-mail address to use for the
supportContact element of this SP.

* `shib_namespace`: This controls the XML namespace string used in the
`xmlns` section at the top of the file `shibboleth2.xml`.  This should be
set to one of the two strings "2.0" or "3.0".  If using Shibboleth SP 3
you should be use "3.0" but if using Shibboleth SP 2 use "2.0". The default
is "2.0" but this will change to "3.0" at some point in the near future.

#### IdP settings

* `idp_entityid`: the URL to IdP.
Default: `https://idp.stanford.edu/`.

* `idp_metadata_url`: the URL to the IdP's metadata.
Default: `https://idp.stanford.edu/metadata.xml`.

* `metadata_backingfile_directory`: where to put the
downloaded IdP's metadata.
Default: `/var/tmp/`.

#### Certificate settings

The SP should be using a public/private key-pair. The
`shibboleth_sp3::shibboleth2_xml` Puppet class expects to find the
certificate file in the `cert_files` module with the name `<cert_name>`
(see below).

* `shib_name`: [**required**] the name used for this Shibboleth instance. This
should normally be the fully-qualified domain name of the host.

* `cert_name`: the name used by the certificate file. This should be the
full name of the file in the `cert_files` containing the certificate.
Default: `${shib_name}-shib`.

* `shib_private_key`: the full path to the SP private key's destination.
Default: `/etc/ssl/private/shib.key`.

* `shib_cert`: the full path to the SP certificate's destination.
Default: `/etc/ssl/certs/shib.pem`.

* `key_wallet_name`: the wallet name containing the SP's private key.
Default: `ssl-key/${shib_name}/shib`.

#### Advanced settings

* `authentication_context`: the authentication context.
Default: `https://refeds.org/profile/mfa`.

* `versioned_data_sealer`: set to `true` if you want to use the versioned
DataSealer (see the [DataSealer wiki
page](https://wiki.shibboleth.net/confluence/display/SP3/DataSealer) for
more information).
Default: `false`.

* `support_saml1`: the default is SAML2 support with SAML1 support
disabled; if you want to also support SAML1 set to `true`.
Default: `false`.

* `attribute_map_path`: the path to the `attribute-map.xml` file (relative to
`/etc/shibboleth2.xml`).
Default: `attribute-map.xml`.

* `manage_attribute_map`: if set to `true` then the
`shibboleth_sp3::shibboleth2_xml` Puppet module will use its own version
of `/etc/shibboleth/attribute-map.xml`. However, if you want to use your
own version in which case you should leave `manage_attribute_map` set to
`false`.
Default: `false`.

* `apache_shib_mod_name`: In older versions of Debian the Shibboleth Apache module
was called "shib2" while in newer versions it is called "shib". If you need to
override the module name use this parameter.
Default: `shib`.


